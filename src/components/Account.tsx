import React from 'react';
import Paper from '@material-ui/core/Paper'
import IconButton from "@material-ui/core/IconButton";
import Avatar from "@material-ui/core/Avatar";
import {createStyles, makeStyles, Theme} from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Axios from 'axios'
import {useSelector} from "react-redux";
import {IRootReducer} from "../redux/IRootReducer";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        accountPaper: {
            width: '45%',
            margin: 'auto'
        },
        usernameText: {
            display: 'inline',
            margin: theme.spacing(1)
        },
        nameText: {
            margin: theme.spacing(1)
        }
    })
);

export default function Account() {

    const classes = useStyles();

    const username: string = useSelector<IRootReducer, string>(
        state => state.usernameReducer.username
    );

    const [name, setName] = React.useState('');

    React.useEffect(() => {
        Axios.get('/user/' + username).then(function (response) {
            setName(response.data);
        });
    }, [username]);

    return (
        <div>
            <Paper variant={'outlined'} square className={classes.accountPaper}>
                <IconButton>
                    <Avatar>{username.substring(0,1)}</Avatar>
                </IconButton>
                <Typography variant={'body1'} className={classes.usernameText}>
                    @{username}
                </Typography>
                <Typography variant={'body1'} className={classes.nameText}>
                    {name}
                </Typography>
            </Paper>
        </div>
    )
}